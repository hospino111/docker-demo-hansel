package com.example.demo.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PersonDao;
import com.example.demo.model.Person;
import com.example.demo.repository.PersonRepository;

import lombok.RequiredArgsConstructor;

@Qualifier("mongoRepository")
@Service
@RequiredArgsConstructor
public class PersonDaoMongoImpl implements PersonDao {

    private final PersonRepository personRepository;

    @Override
    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @Override
    public Person createPerson(Person newPerson) {
        return personRepository.insert(newPerson);
    }
}
