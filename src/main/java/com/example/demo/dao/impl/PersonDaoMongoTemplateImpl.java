package com.example.demo.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PersonDao;
import com.example.demo.model.Person;

import lombok.RequiredArgsConstructor;

@Qualifier("mongoTemplate")
@Service
@RequiredArgsConstructor
public class PersonDaoMongoTemplateImpl implements PersonDao {

    private final MongoTemplate mongoTemplate;

    @Override
    public List<Person> getAllPersons() {
        MongoOperations mongoOps = mongoTemplate;

        return mongoOps.findAll(Person.class);
    }

    @Override
    public Person createPerson(Person newPerson) {
        MongoOperations mongoOps = mongoTemplate;

        return mongoOps.insert(newPerson);
    }
}
