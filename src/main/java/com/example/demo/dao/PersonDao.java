package com.example.demo.dao;

import com.example.demo.model.Person;

import java.util.List;

public interface PersonDao {

    List<Person> getAllPersons();

    Person createPerson(Person newPerson);
}
