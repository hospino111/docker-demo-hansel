package com.example.demo.service;

import com.example.demo.rest.dto.PersonDto;
import com.example.demo.rest.dto.PersonsDto;

public interface PersonService {

    void createPerson(PersonDto newPerson);

    PersonsDto findPersons();
}
