package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PersonDao;

@Qualifier("serviceMongoTemplate")
@Service
public class PersonServiceMongoTemplateImpl extends PersonServiceDefault {

    public PersonServiceMongoTemplateImpl(@Qualifier("mongoTemplate") PersonDao personDao) {
        super(personDao);
    }
}
