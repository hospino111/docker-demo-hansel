package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PersonDao;

@Qualifier("serviceMongo")
@Service
public class PersonServiceImpl extends PersonServiceDefault {

    public PersonServiceImpl(@Qualifier("mongoRepository") PersonDao personDao) {
        super(personDao);
    }
}
