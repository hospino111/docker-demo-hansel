package com.example.demo.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.dao.PersonDao;
import com.example.demo.model.Person;
import com.example.demo.rest.dto.PersonDto;
import com.example.demo.rest.dto.PersonsDto;
import com.example.demo.service.PersonService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class PersonServiceDefault implements PersonService {

    private final PersonDao personDao;

    @Override
    public void createPerson(PersonDto newPerson) {
        Person personEntity = Person.builder()
                .age(newPerson.getAge())
                .name(newPerson.getName())
                .build();

        personDao.createPerson(personEntity);
    }

    @Override
    public PersonsDto findPersons() {
        List<PersonDto> peopleDto = personDao.getAllPersons()
                .stream()
                .map(person -> PersonDto.builder()
                        .age(person.getAge())
                        .name(person.getName())
                        .build())
                .collect(Collectors.toList());
        return PersonsDto.builder().persons(peopleDto).build();
    }
}
