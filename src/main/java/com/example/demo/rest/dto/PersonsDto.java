package com.example.demo.rest.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Builder
@Value
public class PersonsDto {

    private List<PersonDto> persons;
}
