package com.example.demo.rest;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.rest.dto.PersonDto;
import com.example.demo.rest.dto.PersonsDto;
import com.example.demo.service.PersonService;

@RestController
@RequestMapping("/api")
public class PersonController {

    private final PersonService personService;

    public PersonController(@Qualifier("serviceMongo") PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/persons")
    public ResponseEntity<Void> createPerson(@RequestBody PersonDto newPerson) {
        personService.createPerson(newPerson);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/persons")
    public ResponseEntity<PersonsDto> getPersons() {
        return ResponseEntity.ok(personService.findPersons());
    }
}
