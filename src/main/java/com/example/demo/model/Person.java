package com.example.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Document
@Builder
@Value
@ToString
public class Person {
    private String id;
    private String name;
    private int age;
}

