package com.example.demo.dao.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.demo.dao.PersonDao;
import com.example.demo.model.Person;
import com.example.demo.repository.PersonRepository;

@ExtendWith(SpringExtension.class)
class PersonDaoMongoImplTest {

    @Mock
    private PersonRepository personRepository;

    private PersonDao personDao;

    @BeforeEach
    void setup() {
        personDao = new PersonDaoMongoImpl(personRepository);
    }

    @Test
    void shouldGetAllPeople() {
        Person expectedPerson = Person.builder()
                .name("John Doe")
                .age(30)
                .build();
        List<Person> people = Arrays.asList(expectedPerson);

        Mockito.when(personRepository.findAll()).thenReturn(people);

        List<Person> peopleFound = personDao.getAllPersons();

        assertNotNull(peopleFound);
        Assertions.assertFalse(peopleFound.isEmpty());
        Person personFound = peopleFound.get(0);
        assertNotNull(personFound);
        assertEquals(expectedPerson.getName(), personFound.getName());
        assertEquals(expectedPerson.getAge(), personFound.getAge());
    }

    @Test
    void shouldCreatePerson() {
        Person newPerson = Person.builder()
                .name("John Doe")
                .age(30)
                .build();

        Mockito.when(personRepository.insert(newPerson)).thenReturn(newPerson);

        Person personCreated = personDao.createPerson(newPerson);

        assertNotNull(personCreated);
        assertEquals(newPerson.getName(), personCreated.getName());
        assertEquals(newPerson.getAge(), personCreated.getAge());
    }
}
