package com.example.demo.rest;

import com.example.demo.rest.dto.PersonDto;
import com.example.demo.rest.dto.PersonsDto;
import com.example.demo.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
class HelloWorldTest {

    @Mock
    private PersonService personService;

    @InjectMocks
    private HelloWorld helloWorld;

    @Test
    void shouldSayHelloWorld() {
        assertEquals("Hello World", helloWorld.greeting());
    }

    @Test
    void shouldCreatePerson() {
        Mockito.doNothing()
                .when(personService).createPerson(Mockito.any(PersonDto.class));

        ResponseEntity<Void> response = helloWorld.createPerson(PersonDto.builder().build());

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void shouldGetPersons() {
        PersonDto expectedPerson = PersonDto.builder().name("John Doe").age(30).build();
        Mockito.when(personService.findPersons())
                .thenReturn(PersonsDto.builder().persons(Arrays.asList(expectedPerson)).build());

        ResponseEntity response = helloWorld.getPersons();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody() instanceof PersonsDto);
        PersonsDto personsResponse = (PersonsDto) response.getBody();
        assertNotNull(personsResponse.getPersons());
        Assertions.assertFalse(personsResponse.getPersons().isEmpty());
        PersonDto personResponse = personsResponse.getPersons().get(0);
        assertEquals(expectedPerson.getName(), personResponse.getName());
        assertEquals(expectedPerson.getAge(), personResponse.getAge());
    }

    @Test
    void shouldGetPersonsWithNoPeopleFound() {
        Mockito.when(personService.findPersons())
                .thenReturn(PersonsDto.builder().persons(Arrays.asList()).build());

        ResponseEntity response = helloWorld.getPersons();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody() instanceof PersonsDto);
        PersonsDto personsResponse = (PersonsDto) response.getBody();
        assertNotNull(personsResponse.getPersons());
        assertTrue(personsResponse.getPersons().isEmpty());
    }
}
