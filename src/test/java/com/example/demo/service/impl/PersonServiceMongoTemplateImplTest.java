package com.example.demo.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.demo.dao.PersonDao;
import com.example.demo.model.Person;
import com.example.demo.rest.dto.PersonDto;
import com.example.demo.rest.dto.PersonsDto;
import com.example.demo.service.PersonService;

@ExtendWith(SpringExtension.class)
class PersonServiceMongoTemplateImplTest {

    @Mock
    private PersonDao personDao;

    private PersonService personService;

    @BeforeEach
    void setup() {
        personService = new PersonServiceMongoTemplateImpl(personDao);
    }

    @Test
    void shouldCreatePerson() {
        PersonDto personDto = PersonDto.builder()
                .name("John Doe")
                .age(30)
                .build();
        Person newPerson = Person.builder()
                .name("John Doe")
                .age(30)
                .build();

        Mockito.when(personDao.createPerson(newPerson)).thenReturn(newPerson);

        personService.createPerson(personDto);

        Mockito.verify(personDao, Mockito.times(1)).createPerson(newPerson);
    }

    @Test
    void shouldFindPeople() {
        List<Person> people = Arrays.asList(Person.builder()
                .name("John Doe")
                .age(30)
                .build());

        Mockito.when(personDao.getAllPersons())
                .thenReturn(people);

        PersonsDto findPeopleResponse = personService.findPersons();

        assertNotNull(findPeopleResponse);
        assertNotNull(findPeopleResponse.getPersons());
        assertFalse(findPeopleResponse.getPersons().isEmpty());
        PersonDto actualPerson = findPeopleResponse.getPersons().get(0);
        assertEquals("John Doe", actualPerson.getName());
        assertEquals(30, actualPerson.getAge());
    }
}
